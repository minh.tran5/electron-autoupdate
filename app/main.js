const { app, BrowserWindow, dialog, ipcRenderer, ipcMain } = require("electron");
const { autoUpdater } = require("electron-updater");
const appVersion = require('../package.json')
const log = require('electron-log');

autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';
log.info('App starting...');

//app.commandLine.appendSwitch ('disable-http-cache');
//autoUpdater.requestHeaders = {'PRIVATE-TOKEN': 'jsG1GndzZYHqCcbjWEre'};// 'Cache-Control': 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0', 

let win;

const dispatch = (data) => {
  win.webContents.send("message", data);
};

//const getVersion = () => win.webContents.send("versionLbl", appVersion.version);

const createDefaultWindow = () => {
  win = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
    },
  });
  // win.webContents.openDevTools();

  win.on("closed", () => {
    win = null;
  });

  win.loadFile("app/index.html");

  return win;
};





// autoUpdater.setFeedURL({
//   provider: "generic",
//   url: "http://gitlab.com/api/v4/projects/20440444/jobs/artifacts/master/raw/dist/?job=build"
// });
app.commandLine.appendSwitch('disable-http2');
// autoUpdater.requestHeaders = {'Cache-Control' : 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0', 'PRIVATE-TOKEN': 'QMN6kQrHX_L3LGoaeQ-A'};

app.on("ready", () => {
  createDefaultWindow();

  autoUpdater.checkForUpdates();

  win.webContents.on("did-finish-load", () => {
    dispatch(appVersion.version)
  });

});

app.on("window-all-closed", () => {
  app.quit();
});

autoUpdater.requestHeaders = {'Cache-Control' : 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0'};

autoUpdater.autoDownload = true;
autoUpdater.autoInstallOnAppQuit = false;

autoUpdater.on("checking-for-update", () => {
  dispatch("Checking for update...");
});

autoUpdater.on("update-available", (info) => {
  dispatch("Update available.");
});

autoUpdater.on("update-not-available", (info) => {
  dispatch("Update not available.");
});

autoUpdater.on("error", (err) => {
  dispatch("Error in auto-updater. " + err);
});

autoUpdater.on("download-progress", (progressObj) => {
  let log_message = "Download speed: " + progressObj.bytesPerSecond
  log_message = log_message + ' - Downloaded ' + progressObj.percent + '%'
  log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')'
  dispatch(log_message)

  win.webContents.send("download-progress", progressObj.percent);
});

// autoUpdater.on("update-downloaded", (info) => {
//   dispatch("Update downloaded");
//   getVersion();
// });

// autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName) => {
//   const dialogOpts = {
//     type: 'info',
//     buttons: ['Restart', 'Later'],
//     title: 'Application Update',
//     message: process.platform === 'win32' ? releaseNotes : releaseName,
//     detail: 'A new version has been downloaded. Restart the application to apply the updates.'
//   }

//   dialog.showMessageBox(dialogOpts).then((returnValue) => {
//     if (returnValue.response === 0) autoUpdater.quitAndInstall()
//   })
// })

autoUpdater.on('error', message => {
  log.error('There was a problem updating the application')
  log.error({message})
})

process.on('error', err =>{
  log.error({err});
})

ipcMain.on('install', ()=>{
  log.info('start installl')
  autoUpdater.quitAndInstall();
})
